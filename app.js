import bodyParser from 'body-parser'
import express from 'express'
import logger from 'morgan'

// ROUTES
import index from './routes/index'

const app = express()

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

// TODO 3: use the previously created route from /routes/index.js

// catch 404 and forward to error handler
app.use((req, res, next) => {
    const err = new Error('Not Found')
    err.status = 404
    next(err)
})

// error handler
app.use((err, req, res, next) => {
    // set locals, only providing error in development
    if (err.status !== 404) {
        console.error('Express server error:', err)
    }
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    res.status(err.status || 500)
    res.send(`error: ${err.status || 500} <br/> ${err.message} <br/><pre> ${err} </pre>`)
})

export default app
