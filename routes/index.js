// TODO 4: fix linting errors
import express from 'express'

const router = express.Router()

// TODO 2: show 'Hello FlowerAI!' message on the default GET route /

router.all('/favicon.ico', function(req, res, next) {
    res.status(404).send('Error 404');
});

export default router
